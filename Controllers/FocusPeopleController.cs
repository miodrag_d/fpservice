﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using FPService.Helpers;
using FPService.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static FPService.Service.DataService;

namespace FPService.Controllers
{
    public class FocusPeopleController : ApiController
    {
        [AuthorizeIPAddress]
        [HttpGet]
        public JObject Get()
        {
            return DataService.GetProjekter();
           // return DataEncryption.Encrypt(DataService.GetProjekter());
        }
        [AuthorizeIPAddress]
        [HttpGet]
        public JObject GetDataFromFocus()
        {
            Uri myUri = new Uri(Request.RequestUri.AbsoluteUri);
            //get any parameters
            //string projectNumber = HttpUtility.ParseQueryString(myUri.Query).Get("projectNumber");
            //string startDate = HttpUtility.ParseQueryString(myUri.Query).Get("startDate");
            //string endDate = HttpUtility.ParseQueryString(myUri.Query).Get("endDate");
            string path = "data/get";

            var param = this.Request.RequestUri.Query;
            if(!string.IsNullOrEmpty(param.ToString()))
            {
                path += string.Concat(param.ToString());
            }

            return DataService.GetFocusData(path);
           // return Json(result).ToString();
            //return DataService.GetStatusFromFocusAsync();
            // return DataEncryption.Encrypt(DataService.GetProjekter());

        }
        [AuthorizeIPAddress]
        [HttpGet]
        public JObject StatusFromFocus()
        {

            Uri myUri = new Uri(Request.RequestUri.AbsoluteUri);
            //get any parameters
            //string projectNumber = HttpUtility.ParseQueryString(myUri.Query).Get("projectNumber");
            //string startDate = HttpUtility.ParseQueryString(myUri.Query).Get("startDate");
            //string endDate = HttpUtility.ParseQueryString(myUri.Query).Get("endDate");
            string path = "data/status";

            var param = this.Request.RequestUri.Query;
            if (!string.IsNullOrEmpty(param.ToString()))
            {
                path += string.Concat(param.ToString());
            }

            return DataService.GetFocusStatus(path);
            // return Json(result).ToString();
            //return DataService.GetStatusFromFocusAsync();
            // return DataEncryption.Encrypt(DataService.GetProjekter());

        }
        [HttpGet]
        public void UpdateDataFromStatus()
        {

            Uri myUri = new Uri(Request.RequestUri.AbsoluteUri);
            //get any parameters
            //string projectNumber = HttpUtility.ParseQueryString(myUri.Query).Get("projectNumber");
            //string startDate = HttpUtility.ParseQueryString(myUri.Query).Get("startDate");
            //string endDate = HttpUtility.ParseQueryString(myUri.Query).Get("endDate");
            string path = "data/status";

            var param = this.Request.RequestUri.Query;
            if (!string.IsNullOrEmpty(param.ToString()))
            {
                path += string.Concat(param.ToString());
            }

            DataService.UpdateDataFromStatus(path);
            // return Json(result).ToString();
            //return DataService.GetStatusFromFocusAsync();
            // return DataEncryption.Encrypt(DataService.GetProjekter());

        }
        [HttpGet]
        public void UpdateDataFromFocus()
        {

            Uri myUri = new Uri(Request.RequestUri.AbsoluteUri);
            //get any parameters
            //string projectNumber = HttpUtility.ParseQueryString(myUri.Query).Get("projectNumber");
            //string startDate = HttpUtility.ParseQueryString(myUri.Query).Get("startDate");
            //string endDate = HttpUtility.ParseQueryString(myUri.Query).Get("endDate");
            string path = "data/get";

            var param = this.Request.RequestUri.Query;
            if (!string.IsNullOrEmpty(param.ToString()))
            {
                path += string.Concat(param.ToString());
            }

            DataService.UpdateDataFromFocus(path);
            // return Json(result).ToString();
            //return DataService.GetStatusFromFocusAsync();
            // return DataEncryption.Encrypt(DataService.GetProjekter());

        }


    }
}

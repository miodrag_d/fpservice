﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Net.Http.Headers;
using System.Text;
using System.Data;
using System.Web.Script.Serialization;

namespace FPService.Service
{
    public class DataService
    {
        public static object Configuration { get; private set; }

        public static void UpdateDataFromStatus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    // var proj = response.Content.ReadAsAsync<Projekt>();
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //var temp = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    var response2 = JsonConvert.DeserializeObject<FocusStatus>(response.Content.ReadAsStringAsync().Result, settings).Perioder;
                    joStatusRes = JObject.FromObject(response2);

                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }
       
        }

        public static void UpdateDataFromFocus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    List<FocusDataResponse> response1 = JsonConvert.DeserializeObject<List<FocusDataResponse>>(response.Content.ReadAsStringAsync().Result);
                    var strConnect = GetConnectionString();
                    try
                    {

                        using (SqlConnection con = new SqlConnection(strConnect))
                        {
                            con.Open();
                            String procedureName = "ProcedureOpdaterFocusData";
                            using (SqlCommand command = new SqlCommand(procedureName, con))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                con.Open();
                                foreach (FocusDataResponse item in response1)
                                {
                                    //gem data i Focus db
                                    command.Parameters.Add("@hoursAmount", SqlDbType.Int).Value = Convert.ToInt32(item.hoursAmount);
                                    command.Parameters.Add("@hour_type_number", SqlDbType.Int).Value = Convert.ToInt32(item.hour_type_number);
                                    command.Parameters.Add("@hour_type_name", SqlDbType.NVarChar, 250).Value = item.hour_type_name;
                                    command.Parameters.Add("@period", SqlDbType.NVarChar, 250).Value = item.period;
                                    command.Parameters.Add("@project_number", SqlDbType.NVarChar, 250).Value = item.project_number;
                                    command.Parameters.Add("@project_name", SqlDbType.NVarChar, 250).Value = item.project_name;
                                    command.Parameters.Add("@start_date", SqlDbType.DateTime, 250).Value = Convert.ToDateTime(item.start_date);
                                    command.Parameters.Add("@end_date", SqlDbType.DateTime, 250).Value = Convert.ToDateTime(item.end_date);
                                    command.Parameters.Add("@user_name", SqlDbType.NVarChar, 250).Value = item.user_name;
                                    command.Parameters.Add("@created_at", SqlDbType.NVarChar, 250).Value = item.created_at;
                                   
                                        
                                    
                                }
                                command.ExecuteNonQuery();
                                con.Close();

                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                    

                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }

        }
        public static JObject GetProjekter()
        {
            string json = string.Empty;
            JObject jObject = new JObject();
            if (ConfigurationManager.AppSettings["MockUpMode"].ToUpper().ToString() == "TRUE")
            {
                try
                {
                    string JsonFolderLocation = ConfigurationManager.AppSettings["JsonFolder"].ToString();

                    using (StreamReader r = new StreamReader(@JsonFolderLocation))
                    {
                        //json = r.ReadToEnd();
                        var temp = r.ReadToEnd();
                        jObject = JObject.Parse(temp);
                    }
                }
                catch (Exception ex)
                {

                    ex.Message.ToString(); 
                }
                
                return jObject;
            }
                else
                {
                    var strConnect = GetConnectionString();
                    try
                    {
 
                        using (SqlConnection con = new SqlConnection(strConnect))
                        {
                            con.Open();
                            String procedureName = "ProcedureHentFocusData_Normal";
                        using (SqlCommand command = new SqlCommand(procedureName, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            var reader = command.ExecuteReader(); 
                            Projekter projekter = new Projekter();
                            projekter.Projekterelements = new List<ProjekterElement>();
                            while (reader.Read())
                            {                                
                                projekter.Projekterelements.Add(
                                    new ProjekterElement
                                    {
                                        ProjektNavn = reader["projektnavn"].ToString(),
                                        ProjektNummer = Int32.Parse( reader["projektnummer"].ToString()),
                                        ProjektMobilNummer = reader["projektmobilnummer"].ToString(),
                                        Beviling = reader["bevilling"].ToString()
                                        //,
                                        //MedarbejderElements = new List<Medarbejder>
                                        //{
                                        //    new Medarbejder
                                        //    {
                                        //        Navn = reader["navn"].ToString(),
                                        //        Nummer = Int32.Parse( reader["nummer"].ToString()),
                                        //        Mobilnummer = reader["mobilnummer"].ToString()
                                        //    }

                                        //},
                                        
                                        //TimeTyperElements = new List<TimeTyper>
                                        //{
                                        //    new TimeTyper
                                        //    {
                                        //        Id = Int32.Parse(reader["id"].ToString()),
                                        //        Navn = reader["typenavn"].ToString()
                                        //    }
                                        //}


                                    }
                                );
                               
                            }
                            con.Close();
                            jObject = JObject.FromObject(projekter);
                        }
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }

                   return jObject;


                }

        }

        public static JObject GetFocusStatus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;
               
                if (response.IsSuccessStatusCode)
                {
                    // var proj = response.Content.ReadAsAsync<Projekt>();
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //var temp = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    var response2 = JsonConvert.DeserializeObject<FocusStatus>(response.Content.ReadAsStringAsync().Result, settings).Perioder;
                    joStatusRes = JObject.FromObject(response2);
                   
                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }
            return joStatusRes;
        }

        public static JObject GetFocusData(string path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            JObject joRes = new JObject();
            HttpResponseMessage response = client.GetAsync(path).Result;
            if (response.IsSuccessStatusCode)
            {
                // var proj = response.Content.ReadAsAsync<Projekt>();
                //strRes = response.Content.ReadAsStringAsync().Result;
                //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                List<FocusDataResponse> response1 = JsonConvert.DeserializeObject<List<FocusDataResponse>>(response.Content.ReadAsStringAsync().Result);
                //joRes = JObject.FromObject(response1);

                foreach (FocusDataResponse item in response1)
                {
                    joRes = JObject.FromObject(item);
                }

            }
            return joRes;
        }

        public static String GetConnectionString()
        {

            return ConfigurationManager.ConnectionStrings["SQLDB"].ToString();
        }

        public partial class Projekter
        {
            [JsonProperty("projekter")]
            public List<ProjekterElement>  Projekterelements { get; set; }
        }

        public partial class ProjekterElement
        {
            [JsonProperty("projektNavn")]
            public String ProjektNavn { get; set; }

            [JsonProperty("projektNummer")]
            public Int32 ProjektNummer { get; set; }

            [JsonProperty("projektMobilNummer")]
           // [JsonConverter(typeof(ParseStringConverter))]
            public String ProjektMobilNummer { get; set; }

            [JsonProperty("beviling")]
            public String Beviling { get; set; }

            [JsonProperty("medarbejder")]
            public List<Medarbejder> MedarbejderElements { get; set; }

            [JsonProperty("timeTyper")]
            public List <TimeTyper> TimeTyperElements { get; set; }
        }

        public partial class Medarbejder
        {
            [JsonProperty("navn")]
            public String Navn { get; set; }

            [JsonProperty("nummer")]
            public Int32 Nummer { get; set; }

            [JsonProperty("mobilnummer")]
            //[JsonConverter(typeof(ParseStringConverter))]
            public String Mobilnummer { get; set; }
        }

        public partial class TimeTyper
        {
            [JsonProperty("id")]
            public Int32 Id { get; set; }

            [JsonProperty("navn")]
            public String Navn { get; set; }
        }
    }

}

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Net.Http.Headers;
using System.Text;
using System.Data;
using System.Web.Script.Serialization;

namespace FPService.Service
{
    public class DataService
    {
        public static object Configuration { get; private set; }

        public static void UpdateDataFromStatus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    // var proj = response.Content.ReadAsAsync<Projekt>();
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //var temp = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    var response2 = JsonConvert.DeserializeObject<FocusStatus>(response.Content.ReadAsStringAsync().Result, settings).Perioder;
                    joStatusRes = JObject.FromObject(response2);

                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }

        }

        public static void UpdateDataFromFocus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            SqlDataReader reader;
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    //List<FocusDataResponse> response1 = JsonConvert.DeserializeObject<List<FocusDataResponse>>(response.Content.ReadAsStringAsync().Result);

                    JArray jArrResp = JArray.Parse(response.Content.ReadAsStringAsync().Result);
                    var strConnect = GetConnectionString();
                    try
                    {

                        //using (SqlConnection con = new SqlConnection(strConnect))
                        //{
                        //    con.Open();
                        //    String procedureName = "ProcedureOpdaterFocusData";
                        //    using (SqlCommand command = new SqlCommand(procedureName, con))
                        //    {
                        //        command.CommandType = CommandType.StoredProcedure;
                        //        con.Open();
                        //        foreach (FocusDataResponse item in response1)
                        //        {
                        //            //gem data i Focus db
                        //            command.Parameters.Add("@hoursAmount", SqlDbType.Int).Value = Convert.ToInt32(item.hoursAmount);
                        //            command.Parameters.Add("@hour_type_number", SqlDbType.Int).Value = Convert.ToInt32(item.hour_type_number);
                        //            command.Parameters.Add("@hour_type_name", SqlDbType.NVarChar, 250).Value = item.hour_type_name;
                        //            command.Parameters.Add("@period", SqlDbType.NVarChar, 250).Value = item.period;
                        //            command.Parameters.Add("@project_number", SqlDbType.NVarChar, 250).Value = item.project_number;
                        //            command.Parameters.Add("@project_name", SqlDbType.NVarChar, 250).Value = item.project_name;
                        //            command.Parameters.Add("@start_date", SqlDbType.DateTime, 250).Value = Convert.ToDateTime(item.start_date);
                        //            command.Parameters.Add("@end_date", SqlDbType.DateTime, 250).Value = Convert.ToDateTime(item.end_date);
                        //            command.Parameters.Add("@user_name", SqlDbType.NVarChar, 250).Value = item.user_name;
                        //            command.Parameters.Add("@created_at", SqlDbType.NVarChar, 250).Value = item.created_at;



                        //        }
                        //        command.ExecuteNonQuery();
                        //        con.Close();

                        //    }
                        //}
                        //string connString = @"Data Source=localhost\sql2016;Initial Catalog=dwDev;Integrated Security=SSPI";
                        String sprocname = "ProcedureOpdaterFocusData";
                        String paramName = "@json";
                        // Sample JSON string 
                        //string paramValue = "{\"dateTime\":\"2018-03-19T15:15:40.222Z\",\"dateTimeLocal\":\"2018-03-19T11:15:40.222Z\",\"cpuPctProcessorTime\":\"0\",\"memAvailGbytes\":\"28\"}";
                        //String paramValue = St;

                        using (SqlConnection conn = new SqlConnection(strConnect))
                        {
                            conn.Open();

                            //using (SqlCommand cmd = new SqlCommand(sprocname, conn))
                            //{
                            //    // Set command object as a stored procedure
                            //    cmd.CommandType = CommandType.StoredProcedure;
                            //    JObject jObject;
                            //    foreach (var item in response1)
                            //    {
                            //        jObject = JObject.FromObject(item);

                            //        cmd.Parameters.Add(new SqlParameter(paramName, jObject.Root.ToString()));

                            //        cmd.ExecuteNonQuery();

                            //        jObject = null;


                            //    }

                            //    cmd.Dispose();
                            //}
                            using (SqlCommand cmd = new SqlCommand(sprocname, conn))
                            {
                                // Set command object as a stored procedure
                                cmd.CommandType = CommandType.StoredProcedure;
                                JObject jObject;
                                foreach (JObject item in jArrResp.Root)
                                {
                                    // jObject = JObject.FromObject(item);

                                    cmd.Parameters.Add(new SqlParameter(paramName, item.ToString()));

                                    cmd.ExecuteNonQuery();

                                    //jObject = null;


                                }

                                cmd.Dispose();
                            }


                            conn.Close();
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }


                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }

        }
        public static JArray GetProjekter()
        {
            string json = string.Empty;

            if (ConfigurationManager.AppSettings["MockUpMode"].ToUpper().ToString() == "TRUE")
            {
                string JsonFolderLocation = ConfigurationManager.AppSettings["JsonFolder"].ToString();
                JArray jObject = new JArray();
                using (StreamReader r = new StreamReader(@JsonFolderLocation))
                {
                    //json = r.ReadToEnd();
                    var temp = r.ReadToEnd();
                    jObject = JArray.Parse(temp);
                }
                //var tempObject = JsonConvert.DeserializeObject<Object>(json.ToString());
                //var decodedString = JsonConvert.DeserializeObject<string>(json.ToString());
                //json = json.Substring(1, json.Length - 1 );
                //json = json.TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });
                return jObject;
            }
            else
            {
                int capacity = 1000000;
                int maxCapacity = 20000000;
                StringBuilder jsonResult =
                    new StringBuilder(capacity, maxCapacity);
                var strConnect = GetConnectionString();
                try
                {
                    //using (SqlConnection con = new SqlConnection(strConnect))
                    //{
                    //    con.Open();
                    //    string procedureName = "ProcedureHentFocusData";

                    //    using (SqlCommand command = new SqlCommand(procedureName, con))
                    //    {
                    //        command.CommandType = CommandType.StoredProcedure;

                    //        using (SqlDataReader dr = command.ExecuteReader())
                    //        {
                    //            while (dr.Read())
                    //            {
                    //                jsonList.Add(dr.GetString(0));
                    //            }
                    //            dr.Close();                           
                    //        }                 
                    //    }                  
                    //}
                    using (SqlConnection con = new SqlConnection(strConnect))
                    {
                        con.Open();
                        string procedureName = "ProcedureHentFocusData";
                        using (SqlCommand command = new SqlCommand(procedureName, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            //command.Parameters.AddRange(dataRequest.Parameters.ToArray());

                            var reader = command.ExecuteReader(); ;

                            while (reader.Read())
                            {
                                jsonResult.Append(reader.GetValue(0).ToString());
                            }

                            con.Close();
                        }
                    }





                    //return Json(new { success = true, columns = columns.ToArray(), responseText = "Success" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                    //return Json(new { success = false, responseText = "Der skete en fejl i hentning af columns for "}, JsonRequestBehavior.AllowGet);
                }

                //Projekter proj = new Projekter();
                var tempObject = JArray.Parse(jsonResult.ToString());


                //return JsonConvert.SerializeObject(tempObject);
                return tempObject;


            }

        }
        public static JObject GetFocusStatus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    // var proj = response.Content.ReadAsAsync<Projekt>();
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //var temp = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    var response2 = JsonConvert.DeserializeObject<FocusStatus>(response.Content.ReadAsStringAsync().Result, settings).Perioder;
                    joStatusRes = JObject.FromObject(response2);

                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }
            return joStatusRes;
        }

        public static JObject GetFocusData(string path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            JObject joRes = new JObject();
            HttpResponseMessage response = client.GetAsync(path).Result;
            if (response.IsSuccessStatusCode)
            {
                // var proj = response.Content.ReadAsAsync<Projekt>();
                //strRes = response.Content.ReadAsStringAsync().Result;
                //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                List<FocusDataResponse> response1 = JsonConvert.DeserializeObject<List<FocusDataResponse>>(response.Content.ReadAsStringAsync().Result);
                //joRes = JObject.FromObject(response1);

                foreach (FocusDataResponse item in response1)
                {
                    joRes = JObject.FromObject(item);
                }

            }
            return joRes;
        }

        public static string GetConnectionString()
        {

            return ConfigurationManager.ConnectionStrings["SQLDB"].ToString();
        }

        public partial class Projekter
        {
            [JsonProperty("projekter")]
            public ProjekterElement[] ProjekterProjekter { get; set; }
        }

        public partial class ProjekterElement
        {
            [JsonProperty("projektNavn")]
            public string ProjektNavn { get; set; }

            [JsonProperty("projektNummer")]
            public long ProjektNummer { get; set; }

            [JsonProperty("projektMobilNummer")]
            // [JsonConverter(typeof(ParseStringConverter))]
            public long ProjektMobilNummer { get; set; }

            [JsonProperty("beviling")]
            public string Beviling { get; set; }

            [JsonProperty("medarbejder")]
            public Medarbejder[] Medarbejder { get; set; }

            [JsonProperty("timeTyper")]
            public TimeTyper[] TimeTyper { get; set; }
        }

        public partial class Medarbejder
        {
            [JsonProperty("navn")]
            public string Navn { get; set; }

            [JsonProperty("nummer")]
            public long Nummer { get; set; }

            [JsonProperty("mobilnummer")]
            //[JsonConverter(typeof(ParseStringConverter))]
            public long Mobilnummer { get; set; }
        }

        public partial class TimeTyper
        {
            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("navn")]
            public string Navn { get; set; }
        }
    }

}





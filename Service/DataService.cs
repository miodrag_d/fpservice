﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Net.Http.Headers;
using System.Text;
using System.Data;
using System.Web.Script.Serialization;

namespace FPService.Service
{
    public class DataService
    {
        public static object Configuration { get; private set; }

        public static void UpdateDataFromStatus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    // var proj = response.Content.ReadAsAsync<Projekt>();
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //var temp = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    var response2 = JsonConvert.DeserializeObject<FocusStatus>(response.Content.ReadAsStringAsync().Result, settings).Perioder;
                    joStatusRes = JObject.FromObject(response2);

                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }

        }

        public static void UpdateDataFromFocus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    // List<FocusDataResponse> response1 = JsonConvert.DeserializeObject<List<FocusDataResponse>>(response.Content.ReadAsStringAsync().Result);
                    JArray jArrResp = JArray.Parse(response.Content.ReadAsStringAsync().Result);
                    var strConnect = GetConnectionString();
                    try
                    {

                        //using (SqlConnection con = new SqlConnection(strConnect))
                        //{
                        //    con.Open();
                        //    String procedureName = "ProcedureOpdaterFocusData";
                        //    using (SqlCommand command = new SqlCommand(procedureName, con))
                        //    {
                        //        command.CommandType = CommandType.StoredProcedure;
                        //        con.Open();
                        //        foreach (FocusDataResponse item in response1)
                        //        {
                        //            //gem data i Focus db
                        //            command.Parameters.Add("@hoursAmount", SqlDbType.Int).Value = Convert.ToInt32(item.hoursAmount);
                        //            command.Parameters.Add("@hour_type_number", SqlDbType.Int).Value = Convert.ToInt32(item.hour_type_number);
                        //            command.Parameters.Add("@hour_type_name", SqlDbType.NVarChar, 250).Value = item.hour_type_name;
                        //            command.Parameters.Add("@period", SqlDbType.NVarChar, 250).Value = item.period;
                        //            command.Parameters.Add("@project_number", SqlDbType.NVarChar, 250).Value = item.project_number;
                        //            command.Parameters.Add("@project_name", SqlDbType.NVarChar, 250).Value = item.project_name;
                        //            command.Parameters.Add("@start_date", SqlDbType.DateTime, 250).Value = Convert.ToDateTime(item.start_date);
                        //            command.Parameters.Add("@end_date", SqlDbType.DateTime, 250).Value = Convert.ToDateTime(item.end_date);
                        //            command.Parameters.Add("@user_name", SqlDbType.NVarChar, 250).Value = item.user_name;
                        //            command.Parameters.Add("@created_at", SqlDbType.NVarChar, 250).Value = item.created_at;



                        //        }
                        //        command.ExecuteNonQuery();
                        //        con.Close();

                        //    }
                        //}
                        String sprocname = "ProcedureOpdaterFocusData";
                        String paramName = "@json";
                        using (SqlConnection conn = new SqlConnection(strConnect))
                        {
                            conn.Open();
                            using (SqlCommand cmd = new SqlCommand(sprocname, conn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                foreach (var item in jArrResp.Children())
                                {
                                    cmd.Parameters.Clear();
                                    cmd.Parameters.Add(new SqlParameter(paramName, item.ToString()));
                                    cmd.ExecuteNonQuery();
                                    cmd.Dispose();
                                }
                            }
                            conn.Close();
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }


                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }

        }
        public static JObject GetProjekter()
        {
            string json = string.Empty;

            if (ConfigurationManager.AppSettings["MockUpMode"].ToUpper().ToString() == "TRUE")
            {
                string JsonFolderLocation = ConfigurationManager.AppSettings["JsonFolder"].ToString();
                JObject jObject = new JObject();
                using (StreamReader r = new StreamReader(@JsonFolderLocation))
                {
                    //json = r.ReadToEnd();
                    var temp = r.ReadToEnd();
                    jObject = JObject.Parse(temp);
                }
                //var tempObject = JsonConvert.DeserializeObject<Object>(json.ToString());
                //var decodedString = JsonConvert.DeserializeObject<string>(json.ToString());
                //json = json.Substring(1, json.Length - 1 );
                //json = json.TrimStart(new char[] { '"' }).TrimEnd(new char[] { '"' });
                return jObject;
            }
                else
                {
                    int capacity = 1000000;
                    int maxCapacity = 20000000;
                    StringBuilder jsonResult =
                        new StringBuilder(capacity, maxCapacity);
                    var strConnect = GetConnectionString();
                    try
                    {
                        //using (SqlConnection con = new SqlConnection(strConnect))
                        //{
                        //    con.Open();
                        //    string procedureName = "ProcedureHentFocusData";

                        //    using (SqlCommand command = new SqlCommand(procedureName, con))
                        //    {
                        //        command.CommandType = CommandType.StoredProcedure;

                        //        using (SqlDataReader dr = command.ExecuteReader())
                        //        {
                        //            while (dr.Read())
                        //            {
                        //                jsonList.Add(dr.GetString(0));
                        //            }
                        //            dr.Close();                           
                        //        }                 
                        //    }                  
                        //}
                        using (SqlConnection con = new SqlConnection(strConnect))
                        {
                            con.Open();
                            string procedureName = "ProcedureHentFocusData";
                            using (SqlCommand command = new SqlCommand(procedureName, con))
                            {
                                command.CommandType = CommandType.StoredProcedure;

                                //command.Parameters.AddRange(dataRequest.Parameters.ToArray());

                                var reader = command.ExecuteReader(); ;

                                while (reader.Read())
                                {
                                    jsonResult.Append(reader.GetValue(0).ToString());
                                }

                            con.Close();
                             }
                        }





                        //return Json(new { success = true, columns = columns.ToArray(), responseText = "Success" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                        //return Json(new { success = false, responseText = "Der skete en fejl i hentning af columns for "}, JsonRequestBehavior.AllowGet);
                    }

                    //Projekter proj = new Projekter();
                    var tempObject = JObject.Parse(jsonResult.ToString());

                   
                   //return JsonConvert.SerializeObject(tempObject);
                   return tempObject;


                }

        }
        public static JObject GetFocusStatus(String path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Error
            };
            JObject joStatusRes = new JObject();
            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                {
                    // var proj = response.Content.ReadAsAsync<Projekt>();
                    //strRes = response.Content.ReadAsStringAsync().Result;
                    //var temp = response.Content.ReadAsStringAsync().Result;
                    //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                    var response2 = JsonConvert.DeserializeObject<FocusStatus>(response.Content.ReadAsStringAsync().Result, settings).Perioder;
                    joStatusRes = JObject.FromObject(response2);

                }
            }
            catch (JsonSerializationException ex)
            {

                ex.Message.ToString();
            }
            return joStatusRes;
        }

        public static JObject GetFocusData(string path)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://tast.perlauridsen.dk");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            JObject joRes = new JObject();
            HttpResponseMessage response = client.GetAsync(path).Result;
            if (response.IsSuccessStatusCode)
            {
                // var proj = response.Content.ReadAsAsync<Projekt>();
                //strRes = response.Content.ReadAsStringAsync().Result;
                //FocusDataResponse response1 = JsonConvert.DeserializeObject<FocusDataResponse>(response.Content.ReadAsStringAsync().Result);
                List<FocusDataResponse> response1 = JsonConvert.DeserializeObject<List<FocusDataResponse>>(response.Content.ReadAsStringAsync().Result);
                //joRes = JObject.FromObject(response1);

                foreach (FocusDataResponse item in response1)
                {
                    joRes = JObject.FromObject(item);
                }

            }
            return joRes;
        }

        public static string GetConnectionString()
        {

            return ConfigurationManager.ConnectionStrings["SQLDB"].ToString();
        }

        public partial class Projekter
        {
            [JsonProperty("projekter")]
            public ProjekterElement[] ProjekterProjekter { get; set; }
        }

        public partial class ProjekterElement
        {
            [JsonProperty("projektNavn")]
            public string ProjektNavn { get; set; }

            [JsonProperty("projektNummer")]
            public long ProjektNummer { get; set; }

            [JsonProperty("projektMobilNummer")]
           // [JsonConverter(typeof(ParseStringConverter))]
            public long ProjektMobilNummer { get; set; }

            [JsonProperty("beviling")]
            public string Beviling { get; set; }

            [JsonProperty("medarbejder")]
            public Medarbejder[] Medarbejder { get; set; }

            [JsonProperty("timeTyper")]
            public TimeTyper[] TimeTyper { get; set; }
        }

        public partial class Medarbejder
        {
            [JsonProperty("navn")]
            public string Navn { get; set; }

            [JsonProperty("nummer")]
            public long Nummer { get; set; }

            [JsonProperty("mobilnummer")]
            //[JsonConverter(typeof(ParseStringConverter))]
            public long Mobilnummer { get; set; }
        }

        public partial class TimeTyper
        {
            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("navn")]
            public string Navn { get; set; }
        }
    }

}


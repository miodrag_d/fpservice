﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FPService.Service
{
    public class FocusDataResponse
    {
        public string period { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string date { get; set; }
        public string endDate { get; set; }
        public string project_name { get; set; }
        public string project_number { get; set; }
        public string user_name { get; set; }
        public string user_number { get; set; }
        public string hour_type_number { get; set; }
        public string hour_type_name { get; set; }
        public string hoursAmount { get; set; }
    }


    //public class FocusStatusResponse
    //{
    //    public string totalTimer { get; set; }
    //    public string periode { get; set; }
    //    public string medarbejderGodkendtTimer { get; set; }
    //    public string projektGodkendtTimer { get; set; }
    //    public string medarbejderNavn { get; set; }
    //    public string medarbejderNummer { get; set; }
    //    public string projektNavn { get; set; }
    //    public string projektNummer { get; set; }
    //    public string status { get; set; }
    //}

    //public class Status
    //{
    //    //private string temp = "14-09-2019_13-10-2019";
    //    //[JsonProperty(Required = Required.Always)]
    //    //[JsonProperty("14-09-2019_13-10-2019")]
    //    public List<FocusStatusResponse> perioder { get; set; }

    //}






    public partial class FocusStatus
    {
        [JsonProperty("perioder")]
        public Perioder Perioder { get; set; }
    }

    public partial class Perioder
    {
        [JsonProperty("14092019_13102019")]
        public The14092019_13102019[] The14092019_13102019 { get; set; }
    }

    public partial class The14092019_13102019
    {
        [JsonProperty("totalTimer")]
        public string TotalTimer { get; set; }

        [JsonProperty("periode")]
        public string Periode { get; set; }

        [JsonProperty("medarbejderGodkendtTimer")]
        public string MedarbejderGodkendtTimer { get; set; }

        [JsonProperty("projektGodkendtTimer")]
        public string ProjektGodkendtTimer { get; set; }

        [JsonProperty("medarbejderNavn")]
        public string MedarbejderNavn { get; set; }

        [JsonProperty("medarbejderNummer")]
       // [JsonConverter(typeof(ParseStringConverter))]
        public long MedarbejderNummer { get; set; }

        [JsonProperty("projektNavn")]
        public string ProjektNavn { get; set; }

        [JsonProperty("projektNummer")]
        public string ProjektNummer { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}



﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

namespace FPService.Helpers
{
    public class AuthorizeIPAddress : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            //Get users IP Address 
            string ipAddress = HttpContext.Current.Request.UserHostAddress;

            if (ipAddress != null && !IsIpAddressValid(ipAddress.Trim()))
            {
                //Send back a HTTP Status code of 403 Forbidden  
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }

            base.OnActionExecuting(filterContext);
        }

        /// <summary> 
        /// Compares an IP address to list of valid IP addresses attempting to 
        /// find a match 
        /// </summary> 
        /// <param name="ipAddress">String representation of a valid IP Address</param> 
        /// <returns></returns> 
        public static bool IsIpAddressValid(string ipAddress)
        {
            //Split the users IP address into it's 4 octets (Assumes IPv4) 
            var incomingOctets = ipAddress.Trim().Split('.');

            //Get the valid IP addresses from the web.config 
            var addresses =
              Convert.ToString(ConfigurationManager.AppSettings["AuthorizeIPAddresses"]);

            //Store each valid IP address in a string array 
            var validIpAddresses = addresses.Trim().Split(',');

            //Iterate through each valid IP address 
            foreach (var validIpAddress in validIpAddresses)
            {
                //Return true if valid IP address matches the users 
                if (validIpAddress.Trim() == ipAddress)
                {
                    return true;
                }

                //Split the valid IP address into it's 4 octets 
                var validOctets = validIpAddress.Trim().Split('.');

                var matches = !validOctets.Where((t, index) => t != "*" && t != incomingOctets[index]).Any();

                //Iterate through each octet 

                if (matches)
                {
                    return true;
                }
            }

            //Found no matches 
            return false;
        }
    }
}